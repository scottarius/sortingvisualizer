﻿using System.Collections;
using UnityEngine;

public class CoroutineWithData<T>
{
    private IEnumerator m_Target;

    public T Result { get; private set; }

    public Coroutine Coroutine { get; private set; }

    public CoroutineWithData(MonoBehaviour owner, IEnumerator target)
    {
        m_Target = target;
        Coroutine = owner.StartCoroutine(Run());
    }

    private IEnumerator Run()
    {
        while (m_Target.MoveNext())
        {
            if (m_Target.Current.GetType() == typeof(T))
                Result = (T)m_Target.Current;

            yield return m_Target.Current;
        }
    }
}
