﻿using UnityEngine;
using System.Collections;

public class InverseRamp : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        for (int i = 0; i < m_Array.Length; i++)
        {
            int value = (m_Array.Length - 1) - i;

            m_Array[i] = value;

            AddSwap(i, value);
        }

        yield return null;

        NotifySortFinished();

    }
}
