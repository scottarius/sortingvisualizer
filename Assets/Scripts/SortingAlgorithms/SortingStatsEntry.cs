﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingStatsEntry
{
    public enum EntryType { Comparison, Swap }

    public EntryType Type { get; set; }

    public double Milliseconds { get; set; }

    public int IndexA { get; set; }

    public int ValueA { get; set; }

    public int IndexB { get; set; }

    public int ValueB { get; set; }

}
