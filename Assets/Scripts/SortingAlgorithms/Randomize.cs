﻿using UnityEngine;
using System.Collections;

public class Randomize : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        for (int i = 0; i < m_Array.Length; i++)
        {
            int value = Random.Range(0, m_Array.Length);

            m_Array[i] = value;

            AddSwap(i, value);
        }

        yield return null;

        NotifySortFinished();

    }
}
