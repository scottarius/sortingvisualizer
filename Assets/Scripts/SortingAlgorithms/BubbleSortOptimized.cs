﻿using UnityEngine;
using System.Collections;

public class BubbleSortOptimized : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        int i, j, valueA, valueB;
        bool swapped;

        for (i = 0; i < m_Array.Length - 1; i++)
        {
            swapped = false;
            for (j = 0; j < m_Array.Length - i - 1; j++)
            {
                if (CompareGT(j, j + 1))
                {
                    valueB = m_Array[j];
                    valueA = m_Array[j + 1];

                    m_Array[j] = valueA;
                    m_Array[j + 1] = valueB;

                    AddSwap(j, valueA, j + 1, valueB);

                    swapped = true;
                }
            }

            if (swapped == false)
            {
                break;
            }
        }

        yield return null;

        NotifySortFinished();
    }

    
}
