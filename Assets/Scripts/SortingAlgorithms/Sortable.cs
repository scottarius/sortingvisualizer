﻿using UnityEngine;

public class Sortable : MonoBehaviour
{
    public int Value { get; set; }

    public virtual void SetValue(int value, bool reset = false)
    {
        Value = value;
    }

}
