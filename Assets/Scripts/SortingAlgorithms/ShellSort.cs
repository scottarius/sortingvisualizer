﻿using UnityEngine;
using System.Collections;

public class ShellSort : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        int length = m_Array.Length;

        for (int h = length / 2; h > 0; h /= 2)
        {
            for (int i = h; i < length; i += 1)
            {
                int temp = m_Array[i];

                int j;
                for (j = i; j >= h && Compare(j - h, i, temp); j -= h)
                {
                    AddSwap(j, m_Array[j - h]); 
                    m_Array[j] = m_Array[j - h];
                }

                AddSwap(j, temp);
                m_Array[j] = temp;
            }
        }


        yield return null;

        NotifySortFinished();
    }

    private bool Compare(int i, int i2, int val)
    {
        AddComparison(i, i2); 
        return m_Array[i] > val;
    }
}
