﻿using UnityEngine;
using System.Collections;

public class BubbleSort : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        for (int p = 0; p <= m_Array.Length - 2; p++)
        {
            for (int i = 0; i <= m_Array.Length - 2; i++)
            {
                if (CompareGT(i, i + 1))
                {
                    int valueB = m_Array[i + 1];
                    int valueA = m_Array[i];
                    m_Array[i + 1] = valueA;
                    m_Array[i] = valueB;

                    AddSwap(i + 1, valueA, i, valueB);
                }
            }
        }

        yield return null;

        NotifySortFinished();
    }

    
}
