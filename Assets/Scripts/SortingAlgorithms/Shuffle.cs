﻿using UnityEngine;
using System.Collections;

public class Shuffle : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        for (int i = 0; i < m_Array.Length; i++)
        {
            int j = Random.Range(0, m_Array.Length);

            int valueB = m_Array[i];
            int valueA = m_Array[j];

            m_Array[i] = valueA;
            m_Array[j] = valueB;

            AddSwap(i, valueA, j, valueB);
        }

        yield return null;

        NotifySortFinished();

    }
}
