﻿using UnityEngine;
using System.Collections;
public class QuickSort : SortingInterface
{
    protected override IEnumerator DoSort()
    {
        _DoSort(m_Array, 0, m_Array.Length - 1);

        yield return null;

        NotifySortFinished();
    }

    private void _DoSort(int[] array, int low, int high)
    {
        if (low < high)
        {
            int partitionIndex = Partition(array, low, high);

            _DoSort(array, low, partitionIndex - 1);
            _DoSort(array, partitionIndex + 1, high);
        }
    }


    private int Partition(int[] array, int low, int high)
    {
        int pivot = array[high];

        int lowIndex = (low - 1);

        for (int j = low; j < high; j++)
        {
            if (PivotCompare(array, j, high, pivot))
            {
                lowIndex++;

                int valueB = array[lowIndex];
                int valueA = array[j];

                array[lowIndex] = valueA;
                array[j] = valueB;

                AddSwap(lowIndex, valueA, j, valueB);
            }
        }

        int valueB2 = array[lowIndex + 1];
        int valueA2 = array[high]; 
        
        array[lowIndex + 1] = valueA2;
        array[high] = valueB2;

        AddSwap(lowIndex + 1, valueA2, high, valueB2);

        return lowIndex + 1;
    }


    private bool PivotCompare(int[] array, int i1, int i2, int pivot)
    {
        AddComparison(i1, i2);
        return array[i1] <= pivot;
    }

}
