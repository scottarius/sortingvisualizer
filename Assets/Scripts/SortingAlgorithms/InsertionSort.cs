﻿using UnityEngine;
using System.Collections;

public class InsertionSort : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        for (int i = 0; i < m_Array.Length - 1; i++)
        {
            for (int j = i + 1; j > 0; j--)
            {
                if (CompareGT(j -1, j))
                {
                    int valueB = m_Array[j - 1];
                    int valueA = m_Array[j];
                    m_Array[j - 1] = valueA;
                    m_Array[j] = valueB;

                    AddSwap(j - 1, valueA, j, valueB);
                }
            }
        }

        yield return null;

        NotifySortFinished();
    }

}
