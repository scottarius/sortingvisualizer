﻿using UnityEngine;
using System.Collections;

public class BogoSort : SortingInterface
{

    protected override IEnumerator DoSort()
    {
		int iteration = 0;
		while (!IsSorted())
		{
            Remap();
			iteration++;
		}

		yield return null;

        NotifySortFinished();
    }


	private bool IsSorted()
	{
		for (int i = 0; i < m_Array.Length- 1; i++)
		{
			if (m_Array[i] > m_Array[i + 1])
			{
				return false;
			}
		}

		return true;
	}

	private void Remap()
	{
        for (int n = m_Array.Length - 1; n > 0; --n)
        {
            
            int k = Random.Range(0, n + 1);

            int valueB = m_Array[n];
			int valueA = m_Array[k];

			m_Array[n] = valueA;
            m_Array[k] = valueB;

			AddSwap(n, valueA, k, valueB);

		}
    }
}
