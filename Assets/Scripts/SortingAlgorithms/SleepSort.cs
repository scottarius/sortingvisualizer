﻿using UnityEngine;
using System.Collections;

public class SleepSort : SortingInterface
{

    private int m_Count;

    protected override IEnumerator DoSort()
    {
        int length = m_Array.Length;

        for (int i = 0; i < m_Array.Length; i++)
        {
            m_Owner.StartCoroutine(Swap(i, m_Array[i]));
            AddComparison(i, -1);
        }

        yield return null;

        
    }

    private IEnumerator Swap(int i, int value)
    {
        for (int f = 0; f < value + 1; f++)
        {
            yield return new WaitForEndOfFrame();
        }

        m_Array[m_Count] = value;
        AddSwap(m_Count, value);

        m_Count++;

        if (m_Count >= m_Array.Length)
        {
            NotifySortFinished();
        }
    }
}
