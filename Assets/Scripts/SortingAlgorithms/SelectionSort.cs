﻿using UnityEngine;
using System.Collections;

public class SelectionSort : SortingInterface
{

    protected override IEnumerator DoSort()
    {
        int smallest;
        for (int i = 0; i < m_Array.Length - 1; i++)
        {
            smallest = i;

            for (int index = i + 1; index < m_Array.Length; index++)
            {
                if (CompareLT(index, smallest))
                {
                    smallest = index;
                }
            }

            int valueB = m_Array[i];
            int valueA = m_Array[smallest];

            m_Array[i] = valueA;
            m_Array[smallest] = valueB;

            AddSwap(i, valueA, smallest, valueB);
        }

        yield return null;

        NotifySortFinished();
    }
}
