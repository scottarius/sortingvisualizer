﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

public abstract class SortingInterface
{
    public event Action OnSortFinished;

    public List<SortingStatsEntry> Stats = new List<SortingStatsEntry>();

    protected MonoBehaviour m_Owner;
    protected int[] m_Array;

    protected Stopwatch m_Stopwatch;

    public void Sort(int[] arr, MonoBehaviour owner)
    {
        m_Array = arr;
        m_Owner = owner;

        m_Stopwatch = new Stopwatch();
        m_Stopwatch.Start();

        m_Owner.StartCoroutine(DoSort());
    }

    protected abstract IEnumerator DoSort();

    protected bool CompareGT(int i1, int i2)
    {
        AddStat(SortingStatsEntry.EntryType.Comparison, i1, i2);
        return (m_Array[i1] > m_Array[i2]);
    }

    protected bool CompareLT(int i1, int i2)
    {
        AddStat(SortingStatsEntry.EntryType.Comparison, i1, i2);
        return (m_Array[i1] < m_Array[i2]);
    }

    protected bool CompareEQ(int i1, int i2)
    {
        AddStat(SortingStatsEntry.EntryType.Comparison, i1, i2);
        return (m_Array[i1] == m_Array[i2]);
    }

    protected void AddSwap(int indexA, int valueA, int indexB = -1, int valueB = -1)
    {
        AddStat(SortingStatsEntry.EntryType.Swap, indexA, valueA, indexB, valueB);
    }

    protected void AddComparison(int indexA, int indexB)
    {
        AddStat(SortingStatsEntry.EntryType.Comparison, indexA, -1, indexB, -1);
    }

    protected void AddStat(SortingStatsEntry.EntryType type, int indexA, int valueA, int indexB = -1, int valueB = -1)
    {
        Stats.Add(new SortingStatsEntry()
        {
            Type = type,
            Milliseconds = m_Stopwatch.Elapsed.TotalMilliseconds,
            IndexA = indexA,
            ValueA = valueA,
            IndexB = indexB,
            ValueB = valueB
        });
    }

    protected void NotifySortFinished()
    {
        if (null != OnSortFinished)
        {
            OnSortFinished();
        }
    }
}
