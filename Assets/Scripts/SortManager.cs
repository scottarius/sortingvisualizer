﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SortManager : MonoBehaviour
{
    public enum SortingAlgorithms { Shuffle, QuickSort, BubbleSort, BubbleSortOptimized, SleepSort, ShellSort, InsertionSort, SelectionSort, BogoSort };
    public enum ValuesTypes { LinearShuffled, LinearReverse, Randomized };

    public int m_ElementCount = 100;
    public int m_MaxHeight = 10;

    public Element m_ElementPrefab;

    public CameraHelper m_Camera;

    public Text m_NameText;
    public Text m_ComparisonsText;
    public Text m_SwapsText;
    public Text m_TimeText;

    public Color m_DefaultElementColor;

    public CanvasGroup m_SpaceHint;
    public CanvasGroup m_StatsGroup;

    public ValuesTypes m_ValuesType;

    
    public float ScaleIncrement
    {
        get
        {
            return m_MaxHeight / (float)m_ElementCount;
        }
    }

    private Bounds m_Bounds;
    private Element[] m_Elements;

    private SortingInterface m_Algorithm;

    private Queue<SortingAlgorithms> m_SortQueue;

    private SortingAlgorithms m_CurrentSort;

    private bool m_CanSkip = false;

    private int m_ComparisonsTally;
    private int m_SwapsTally;
    private double m_TicksTally;
    
    void Start()
    {
        m_NameText.text = "";
        m_StatsGroup.alpha = 0;
        
        StartCoroutine(Init());
    }

    IEnumerator Init()
    {
        yield return new WaitForSeconds(2.0f);

        Spawn();

        yield return new WaitForSeconds(2.0f);

        m_SortQueue = new Queue<SortingAlgorithms>();

        ProcessQueue();
    }

    private void Update()
    {
        if (m_CurrentSort != SortingAlgorithms.Shuffle && m_Algorithm != null)
        {
            m_StatsGroup.alpha = 1;
            m_ComparisonsText.text = m_ComparisonsTally.ToString("N0");
            m_SwapsText.text = m_SwapsTally.ToString("N0");
            m_TimeText.text = m_TicksTally.ToString() + "ms";
        }
        else
        {
            m_StatsGroup.alpha = 0;
        }

        m_SpaceHint.alpha = m_CanSkip ? 1 : 0;

        if (Input.GetKeyDown(KeyCode.Space) && m_CanSkip)
        {
            SkipToNext();
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position + m_Bounds.center, m_Bounds.size);
    }

    public void OnValuesTypeChanged(int index)
    {
        m_ValuesType = (ValuesTypes)index;
        
        if (m_CurrentSort == SortingAlgorithms.Shuffle)
        {
            RestartCurrent();
        }
        else
        {
            SortingAlgorithms[] oldQueue = new SortingAlgorithms[m_SortQueue.Count];
            m_SortQueue.CopyTo(oldQueue, 0);
            m_SortQueue.Clear();

            m_SortQueue.Enqueue(SortingAlgorithms.Shuffle);
            m_SortQueue.Enqueue(m_CurrentSort);
            foreach (SortingAlgorithms item in oldQueue)
            {
                m_SortQueue.Enqueue(item);
            }

            SkipToNext();
        }

        GUI.SetNextControlName("");
        GUI.FocusControl("");
    }

    void ProcessQueue()
    {
        m_ComparisonsTally = 0;
        m_SwapsTally = 0;
        m_TicksTally = 0;

        if (m_SortQueue.Count == 0)
        {
            foreach (SortingAlgorithms al in System.Enum.GetValues(typeof(SortingAlgorithms)))
            {
                if (al != SortingAlgorithms.Shuffle && al != SortingAlgorithms.BogoSort)
                {
                    m_SortQueue.Enqueue(SortingAlgorithms.Shuffle);
                    m_SortQueue.Enqueue(al);
                }
            }
        }


        m_CurrentSort = m_SortQueue.Dequeue();
        Sort(m_CurrentSort);
    }

    void SkipToNext()
    {
        m_CanSkip = false;
        m_NameText.text = "";

        StopAllCoroutines();
        m_Algorithm = null;

        StartCoroutine(_SkipToNext());
    }

    IEnumerator _SkipToNext()
    {
        yield return new WaitForSeconds(0.5f);

        ProcessQueue();
    }

    void RestartCurrent()
    {
        m_CanSkip = false;
        m_NameText.text = "";

        StopAllCoroutines();
        m_Algorithm = null;

        Sort(m_CurrentSort);
    }

    void Spawn()
    {
        m_Elements = new Element[m_ElementCount];

        float scaleIncrement = m_MaxHeight / (float)m_ElementCount;

        for (int i = 0; i < m_ElementCount; i++)
        {
            Element newElement = Instantiate<Element>(m_ElementPrefab);
            
            newElement.Init(this);
            newElement.SetValue(i);
            newElement.transform.parent = transform;

            m_Elements[i] = newElement;
        }

        RefreshOrder();
    }

    private void Reset()
    {
        for (int i = 0; i < m_ElementCount; i++)
        {
            m_Elements[i].SetValue(i);
        }
    }

    void RefreshOrder()
    {
        for (int i = 0; i < m_ElementCount; i++)
        {
            m_Elements[i].transform.position = new Vector3((0.1f * i) * 1f, 0, 0);
        }

        RecalculateBounds();
    }

    void RecalculateBounds()
    {
        m_Bounds = new Bounds();
        AddChildrenToBounds(transform);

        m_Camera.m_Target = m_Bounds;
    }

    void AddChildrenToBounds(Transform child)
    {

        foreach (Transform grandChild in child)
        {

            if (grandChild.GetComponent<Renderer>())
            {
                m_Bounds.Encapsulate(grandChild.GetComponent<Renderer>().bounds.min);
                m_Bounds.Encapsulate(grandChild.GetComponent<Renderer>().bounds.max);
            }

            AddChildrenToBounds(grandChild);

        }

        
    }


    void Sort(SortingAlgorithms algorithm)
    {
        m_CanSkip = true;
        
        switch (algorithm)
        {
            case SortingAlgorithms.Shuffle:
                {
                    m_CanSkip = false; 
                    
                    switch  (m_ValuesType)
                    {
                        case ValuesTypes.LinearReverse:
                            {
                                m_Algorithm = new InverseRamp();
                                break;
                            }
                        case ValuesTypes.Randomized:
                            {
                                m_Algorithm = new Randomize();
                                break;
                            }
                        default:
                            {
                                Reset();
                                m_Algorithm = new Shuffle();
                                break;
                            }
                    }

                    m_NameText.text = "Shuffling...";
                    break;
                }
            case SortingAlgorithms.QuickSort:
                {
                    m_Algorithm = new QuickSort();

                    m_NameText.text = "Quick Sort";

                    break;
                }
            case SortingAlgorithms.BubbleSort:
                {
                    m_Algorithm = new BubbleSort();

                    m_NameText.text = "Bubble Sort";

                    break;
                }
            case SortingAlgorithms.BubbleSortOptimized:
                {
                    m_Algorithm = new BubbleSortOptimized();

                    m_NameText.text = "Bubble Sort Optimized";

                    break;
                }
            case SortingAlgorithms.ShellSort:
                {
                    m_Algorithm = new ShellSort();

                    m_NameText.text = "Shell Sort";

                    break;
                }
            case SortingAlgorithms.SleepSort:
                {
                    m_Algorithm = new SleepSort();

                    m_NameText.text = "Sleep Sort";

                    break;
                }
            case SortingAlgorithms.BogoSort:
                {
                    m_Algorithm = new BogoSort();

                    m_NameText.text = "Bogo Sort";

                    break;
                }
            case SortingAlgorithms.InsertionSort:
                {
                    m_Algorithm = new InsertionSort();

                    m_NameText.text = "Insertion Sort";

                    break;
                }
            case SortingAlgorithms.SelectionSort:
                {
                    m_Algorithm = new SelectionSort();

                    m_NameText.text = "Selection Sort";

                    break;
                }
            default:
                {
                    m_Algorithm = null;
                    break;
                }
        }


        if (null == m_Algorithm)
        {
            return;
        }


        m_Algorithm.OnSortFinished += M_Algorithm_OnSortFinished;


        int[] toSort = new int[m_ElementCount];
        for(int i = 0; i < m_ElementCount; i++)
        {
            toSort[i] = m_Elements[i].Value;
        }

        m_Algorithm.Sort(toSort, this);
    }


    private void M_Algorithm_OnSortFinished()
    {
        StartCoroutine(AnimateSort());
    }

    private IEnumerator AnimateSort()
    {
        foreach (SortingStatsEntry stat in m_Algorithm.Stats)
        {
            Color color = Color.yellow;

            
            if (stat.Type == SortingStatsEntry.EntryType.Comparison)
            {
                m_ComparisonsTally++;
            }
            else
            {
                m_SwapsTally++;
            }
            m_TicksTally = stat.Milliseconds;


            if (m_CurrentSort != SortingAlgorithms.Shuffle)
            {
                color = stat.Type == SortingStatsEntry.EntryType.Swap ? Color.red : Color.cyan;
            }

            if (stat.IndexA >= 0 && stat.IndexA < m_ElementCount)
            {
                m_Elements[stat.IndexA].SetColor(color, true, stat.Type == SortingStatsEntry.EntryType.Swap);

                if (stat.Type == SortingStatsEntry.EntryType.Swap && stat.ValueA >= 0)
                {
                    m_Elements[stat.IndexA].SetValue(stat.ValueA);
                }
            }

            if (stat.IndexB >= 0 && stat.IndexB < m_ElementCount)
            {
                m_Elements[stat.IndexB].SetColor(color, true, stat.Type == SortingStatsEntry.EntryType.Swap);

                if (stat.Type == SortingStatsEntry.EntryType.Swap && stat.ValueB >= 0)
                {
                    m_Elements[stat.IndexB].SetValue(stat.ValueB);
                }
            }

            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(1.0f);


        if (m_CurrentSort != SortingAlgorithms.Shuffle)
        {
            for (int i = 0; i < m_ElementCount; i++)
            {
                m_Elements[i].SetColor(Color.green);
                yield return null;
            }
        }


        yield return new WaitForSeconds(1.0f);
        for (int i = 0; i < m_ElementCount; i++)
        {
            m_Elements[i].Reset();
        }


        ProcessQueue();

    }


}
