﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHelper : MonoBehaviour
{
    public Camera m_Camera;
    public Bounds m_Target;
    public Vector3 m_Velocity = Vector3.zero;
    public float m_SmoothTime = 0.3f;
    public float m_DriftSpeed = 1;
    public float m_DriftXScale = 1;
    public float m_DriftYScale = 1;
    public float m_YOffset = 0;

    void Update()
    {
        if (null != m_Target)
        {
            Vector3 xyz = m_Target.size;
            float distance = Mathf.Max(xyz.x, xyz.y, xyz.z);
            distance /= (2.0f * Mathf.Tan(m_Camera.fieldOfView * Mathf.Deg2Rad));
            
            Vector3 targetPosition = new Vector3(m_Target.center.x, m_Target.center.y + m_YOffset, -distance * 2.0f);
            
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref m_Velocity, m_SmoothTime);
                
        }

        m_Camera.transform.localPosition = (Vector3.right * Mathf.Sin(Time.timeSinceLevelLoad / 2 * m_DriftSpeed) * m_DriftXScale - Vector3.up * Mathf.Sin(Time.timeSinceLevelLoad * m_DriftSpeed) * m_DriftYScale);
        m_Camera.transform.LookAt(m_Target.center);
    }


    

}
