﻿using UnityEngine;

public class Element : Sortable
{
    public Renderer m_Renderer;
    private bool m_DoReset = false;
    private float m_ResetProgress = 0;
    private Color m_ResetFromColor;
    private SortManager m_SortManager;
    private bool m_HasPriority = false;

    public void Init(SortManager sortManager)
    {
        m_SortManager = sortManager;

        Reset();
    }

    public override void SetValue(int value, bool reset = false)
    {
        Value = value;
        transform.localScale = new Vector3(transform.localScale.x, Value * m_SortManager.ScaleIncrement, transform.localScale.z);

        if (reset)
        {
            Reset();
        }
    }

    private void Update()
    {
        if (m_DoReset && m_ResetProgress < 1)
        {
            m_ResetProgress += m_HasPriority ? .07f : .07f; 
            m_Renderer.material.SetColor("_Color", Color.Lerp(m_ResetFromColor, m_SortManager.m_DefaultElementColor, m_ResetProgress));
        }
        else
        {
            if (m_HasPriority)
            {
                m_HasPriority = false;
            }
        }
    }

    public void Reset()
    {
        m_ResetProgress = 0;
        m_DoReset = true;
        m_ResetFromColor = m_Renderer.material.GetColor("_Color");

    }

    public void SetColor(Color color, bool reset = false, bool priority = false)
    {
        if (!m_HasPriority)
        {
            m_HasPriority = priority;
            
            if (null != m_Renderer)
            {
                m_DoReset = false;
                m_Renderer.material.SetColor("_Color", color);
            }

            if (reset)
            {
                Reset();
            }
        }
    }
}
